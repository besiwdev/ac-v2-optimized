import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { openSignInModal } from '@/state/action'
import { setFollowPlaylist } from '@/state/apiActions'
import { followedPlaylistsSelector, loggedInSelector } from '@/state/selectors/user'

export type IFollowStatus = "loading" | "true" | "false"
interface IFetchPost {
    id: string,
    className?: string
    render: (data: { followed: IFollowStatus }) => JSX.Element
}

const FollowTopic: React.FC<IFetchPost> = ({ id, className, render }) => {
    const loggedIn = useSelector(loggedInSelector)
    const followedPlaylists = useSelector(followedPlaylistsSelector)

    const [followed, setFollowed] = React.useState<IFollowStatus>("loading")
    const dispatch = useDispatch()

    React.useEffect(() => {

        const found = followedPlaylists.findIndex(p => p.id === id)
        const followed = found > -1

        setFollowed(followed ? "true" : "false")
    }, [id, followedPlaylists])

    const handleClick = () => {
        if (loggedIn === "success") {
            if (followed !== "loading") {
                setFollowed("loading")
                setFollowPlaylist(dispatch, { id, followed: followed === "true" })
            }
        } else {
            dispatch(openSignInModal("signInOptions"))
        }
    }

    return (
        <div
            className={className ? className : ''}
            onClick={handleClick}
            onKeyDown={handleClick}
        >
            {render({ followed })}
        </div>
    )
}

export default FollowTopic