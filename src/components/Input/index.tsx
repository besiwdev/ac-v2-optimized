import Text from './Text'
import Checkbox from './Checkbox'
import TextArea from './TextArea'
export const InputText = Text
export const InputCheckbox = Checkbox
export const InputTextArea = TextArea 