'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.keyboardControls = exports.getVendor = exports.formatTime = undefined;

var _formatTime2 = require('./format-time');

var _formatTime3 = _interopRequireDefault(_formatTime2);

var _getVendor2 = require('./get-vendor');

var _getVendor3 = _interopRequireDefault(_getVendor2);

var _keyboardControls2 = require('./keyboard-controls');

var _keyboardControls3 = _interopRequireDefault(_keyboardControls2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.formatTime = _formatTime3.default;
exports.getVendor = _getVendor3.default;
exports.keyboardControls = _keyboardControls3.default;