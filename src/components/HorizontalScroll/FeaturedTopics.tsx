import React from 'react'
import XScrollCustomSize from './BaseCustomSize'
import FollowTopic from '@/components/Cards/FollowTopic'
import { useSelector } from 'react-redux'
import { IRootState } from '@/state/types'
import './horizontal-scroll.css';
import { ITopic } from '@/types'
import shortid from 'shortid'
import { followedTopicsSelector, loggedInSelector } from '@/state/selectors/user'

const FeatureSection: React.FC<{ featured: ITopic[] }> = ({ featured }) => {
    const loggedIn = useSelector(loggedInSelector)
    const followedTopics = useSelector(followedTopicsSelector)

    let list = [...featured]
    if (loggedIn === "success") {
        const filtered = featured.filter(t => {
            const find = followedTopics.find(ft => `${ft.id}` === `${t.id}`)

            return find === undefined
        })
        list = filtered
    }
    return (
        <div>
            <XScrollCustomSize
                childeClassName=""
                items={list.map((topic) => (
                    <FollowTopic {...topic} key={shortid()} />
                ))}
            />
        </div>
    )
}

export default FeatureSection