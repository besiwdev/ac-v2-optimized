import { Dispatch, Action } from 'redux'
import { ITrack, IUser, IMedia, ITrackType, INavItem, ITranslations, IApiItem, IBreadcrumb } from '@/types'


export interface StateAction extends Action {
  dispatch: Dispatch
  payload?: any
}

export interface IRootState {
  breadcrumb: IBreadcrumb
  userLibrary: IUserLibrary
  auth: IUserState,
  translatedUrls: INavItem[]
  isSignInModalOpen: ISignInModalContentType,
  currentMedia: IMedia,
  playlist: IMedia[]
  isAutoPlay: boolean,
  mpPlayPause: boolean

}

export interface IUserLibrary {
  followedTopics: IApiItem[]
  followedPlaylists: IApiItem[]
  followedAuthors: IApiItem[]
  unfinishedPosts: IApiItem[]
  bookmarkedPosts: IApiItem[]
  historyPosts: IApiItem[]
}

export type ILogginStatus = 'success' | 'loading' | 'notLoggedIn'
export interface IUserState {
  is_editor?: boolean
  user?: IUser
  loggedIn: ILogginStatus
  errorMessage?: string
}

export type ISignInModalContentType = 'signUpOptions' | 'signInOptions' | 'signInForm' | 'signUpForm' | "forgotPassword" | "giveConsent" | null

export interface ADD_T_URLS_Payload {
  translated: INavItem[]
}


