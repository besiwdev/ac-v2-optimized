
import { Dispatch } from 'redux'

import { setUser, setLogInError, setRegisterError } from '@/state/action/authAction'
import {
    setUserBookmarked,
    setUserHistory,
    setUserFollowingTopics,
    setUserUnfinished,
    setUserLibrary,
    setUserFollowingPlaylists
} from '@/state/action/userAction'
import { closeSignInModal, openSignInModal } from '@/state/action'

import ac_strings from '@/static/strings/ac_strings.js'

import { IUserLibrary } from '../types'

import { IHistory, IBookmarked, IUnfinished, IFollowing, IUser } from '@/types'

import acApi from '@/util/api'

export const checkUser = (dispatch: Dispatch) => {
    return acApi
        .profile()
        .then((res: IUser) => {
            if (res && res.id) {
                if (res.meta && res.meta.consented) {
                    dispatch(setUser(res))

                    dispatch(closeSignInModal())
                    return getUserLibrary(dispatch)
                } else {
                    dispatch(openSignInModal("giveConsent"))
                }
            } else {
                initiateLogout(dispatch)
            }
        })
        .catch((err: any) => {
            console.log(err)
            initiateLogout(dispatch)
            console.log('handle login error')
        })
}

export const initiateConsentNotify = (dispatch: Dispatch, payload: any) => {
    const { receivedEmail } = payload
    return acApi.toggleNotifyAndGiveConsent(receivedEmail)
        .then(res => {
            if (res.consent.success) {
                return acApi.profile().then(userRes => {
                    if (userRes && userRes.meta && userRes.meta.consented) {
                        dispatch(setUser(userRes))

                        dispatch(closeSignInModal())
                        return getUserLibrary(dispatch)
                    } else {
                        dispatch(openSignInModal("signInForm"))
                    }
                    /* return store.dispatch(setUser(userRes)) */
                })
            } else {
                initiateLogout(dispatch)
                dispatch(setLogInError('Something'))
            }

        })
}
//initiateLogIn

export const initiateLogIn = (dispatch: Dispatch, payload: any) => {
    const { email: login_email, password: login_password, remember } = payload
    return acApi.login(login_email, login_password, remember)
        .then((res: any) => {
            if (res) {
                const data = res.signIn
                if (data.success && data.user) {

                    if (data.user.meta && data.user.meta.consented) {
                        dispatch(setUser(data.user))
                        dispatch(closeSignInModal())
                        dispatch(setLogInError(''))
                    } else {
                        dispatch(setLogInError(''))
                        dispatch(openSignInModal("giveConsent"))
                    }

                } else {

                    throw new Error(ac_strings.error_email_password)
                }
            } else {
                throw new Error(ac_strings.error_email_password)
            }


        })
        .catch((err: any) => {
            const message = err[0] || err.message
            initiateLogout(dispatch)
            dispatch(setLogInError(message))
        })
}

export const initiateRegister = (dispatch: Dispatch, payload: any) => {
    const { email: register_email, password: register_password, } = payload

    return acApi
        .register(register_email, register_password, false)
        .then((UserRes: any) => {
            if (UserRes && UserRes.signUp && UserRes.signUp.user) {
                dispatch(setUser(UserRes.signUp.user))
                if (UserRes.signUp.user.meta && UserRes.signUp.user.meta.consented) {
                    dispatch(closeSignInModal())
                } else {
                    dispatch(openSignInModal("signInForm"))
                }

            } else {
                dispatch(setRegisterError(ac_strings.error_something_went_wrong))
            }
        })
        .catch((err: any) => {
            const message = err[0] || err.message
            initiateLogout(dispatch)
            dispatch(setRegisterError(message))
        })
}

export const initiateLogout = (dispatch: Dispatch) => {
    return acApi
        .logout()
        .then(() => {
            initiateLogout(dispatch)
        })
        .catch((err: any) => {
            console.log(err)
            dispatch(setRegisterError(err.message))
        })
}


export const setNewFollowTopic = (dispatch: Dispatch, payload: any) => {
    return acApi
        .followTopic(payload.id, payload.followed)
        .then((resNewFollow: any) => {

            return acApi
                .following()
                .then((res: IFollowing) => {

                    if (res.following && Array.isArray(res.following.topics)) {

                        const filtered = res.following.topics.filter(p => typeof p.id === "string")
                        dispatch(setUserFollowingTopics(filtered))

                    } else {
                        dispatch(setUserFollowingTopics([]))
                    }

                })
                .catch((error: any) => {
                    console.log(error)
                })

        }).catch((error: any) => {
            console.log(error)
        })
}

export const setFollowPlaylist = (dispatch: Dispatch, payload: any) => {
    return acApi
        .followPlaylist(payload.id, payload.followed)
        .then((resNewFollow: any) => {
            return acApi
                .following()
                .then((res: IFollowing) => {
                    if (res.following && Array.isArray(res.following.playlists)) {

                        const filtered = res.following.playlists.filter(p => typeof p.id === "string")
                        dispatch(setUserFollowingPlaylists(filtered))
                    } else {

                        dispatch(setUserFollowingPlaylists([]))
                    }

                })
                .catch((error: any) => {
                    console.log(error)
                })

        }).catch((error: any) => {
            console.log(error)
        })
}

export const setNewBookmark = (dispatch: Dispatch, payload: any) => {
    return acApi
        .bookmarkPost(payload.id, !payload.bookmarked)
        .then((resNewBookmark: any) => {

            if (resNewBookmark.bookmarkPost && resNewBookmark.bookmarkPost.success === true) {
                return acApi.bookmarked()
                    .then((res: IBookmarked) => {

                        if (Array.isArray(res.bookmarked)) {
                            const filtered = res.bookmarked.filter(p => typeof p.id === "string")
                            dispatch(setUserBookmarked(filtered))
                        }
                    })
            } else {

                throw Error('feil to set new like')
            }

        })
        .catch((err: any) => {
            console.log(err)
        })
}

export const getFollowing = (dispatch: Dispatch) => {
    return acApi
        .following()
        .then((res: IFollowing) => {
            if (Array.isArray(res.following.topics)) {
                if (res.following.topics) {
                    const filtered = res.following.topics.filter(p => typeof p.id === "string")
                    dispatch(setUserFollowingTopics(filtered))
                }
            }

        })
        .catch((err: any) => {
            // set_FOLLOWING_ERROR
        })
}

export const getHistory = (dispatch: Dispatch) => {
    return acApi
        .history()
        .then((res: IHistory) => {
            if (Array.isArray(res.history)) {
                const filtered = res.history.filter(p => typeof p.id === "string")
                dispatch(setUserHistory(filtered))
            }

        })
        .catch((err: any) => {
            console.log(err)
            // set_FOLLOWING_ERROR
        })
}

export const getBookmarks = (dispatch: Dispatch) => {
    return acApi
        .bookmarked()
        .then((res: IBookmarked) => {
            if (Array.isArray(res.bookmarked)) {
                const filtered = res.bookmarked.filter(p => typeof p.id === "string")
                dispatch(setUserBookmarked(filtered))
            }

        })
        .catch((err: any) => {
            // set_FOLLOWING_ERROR
        })
}

export const getUnfinished = (dispatch: Dispatch) => {
    return acApi
        .unfinishedPosts()
        .then((res: IUnfinished) => {
            if (Array.isArray(res.unfinishedPosts)) {
                const filtered = res.unfinishedPosts.filter(p => typeof p.id === "string")
                dispatch(setUserUnfinished(filtered))
            }
        })
        .catch((err: any) => {
            console.log(err)
            // set_FOLLOWING_ERROR
        })
}

export const getUserLibrary = (dispatch: Dispatch) => {
    return Promise.all([
        acApi.bookmarked()
            .then((res: IBookmarked) => {
                if (Array.isArray(res.bookmarked)) {
                    return res.bookmarked
                } else {
                    throw new Error('Error res.bookmarked')
                }
            })
            .catch((err: any) => {
                // set_FOLLOWING_ERROR
            }), //0
        acApi
            .following()
            .then((res: IFollowing) => {
                if (Array.isArray(res.following.topics)) {
                    if (res.following) {
                        return res.following
                    } else {
                        throw new Error('Error res.topics')
                    }
                }

            })
            .catch((err: any) => {
                // set_FOLLOWING_ERROR
            }), //3
        acApi
            .history()
            .then((res: IHistory) => {
                if (Array.isArray(res.history)) {
                    return res.history
                } else {
                    throw new Error('Error res.history')
                }

            })
            .catch((err: any) => {
                console.log(err)
                // set_FOLLOWING_ERROR
            }), //4
        acApi
            .unfinishedPosts()
            .then((res: IUnfinished) => {
                if (Array.isArray(res.unfinishedPosts)) {
                    return res.unfinishedPosts
                } else {
                    throw new Error('Error res.unfinishedPosts')
                }
            })
            .catch((err: any) => {
                console.log(err)
                // set_FOLLOWING_ERROR
            }) //5

    ])
        .then(res => {
            const userLibrary: IUserLibrary = {
                bookmarkedPosts: [],
                followedTopics: [],
                historyPosts: [],
                unfinishedPosts: [],
                followedAuthors: [],
                followedPlaylists: []
            }
            if (res[0] && res[0].length > 0) {
                userLibrary.bookmarkedPosts = res[0].filter(p => typeof p.id === "string")
            }

            if (res[1]) {
                userLibrary.followedTopics = res[1].topics.filter(p => typeof p.id === "string")
                userLibrary.followedPlaylists = res[1].playlists.filter(p => typeof p.id === "string")
                userLibrary.followedAuthors = res[1].authors.filter(p => typeof p.id === "string")
            }

            if (res[2] && res[2].length > 0) {
                userLibrary.historyPosts = res[2].filter(p => typeof p.id === "string")
            }

            if (res[3] && res[3].length > 0) {
                userLibrary.unfinishedPosts = res[3].filter(p => typeof p.id === "string")
            }
            console.log(userLibrary)
            dispatch(setUserLibrary(userLibrary))

            //)
        })
}

const all = {
    initiateConsentNotify,
    initiateLogIn,
    initiateRegister,
    initiateLogout,
    getUserLibrary,
    getBookmarks,
    getHistory,
    setNewBookmark,
    setFollowPlaylist,
    setNewFollowTopic
}
export default all