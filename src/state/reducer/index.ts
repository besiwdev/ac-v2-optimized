import { combineReducers } from "redux";
import breadcrumb from './breadcrumb'
import playlist from "./mp_playlist"
import isAutoPlay from "./mp_isAutoPlay"
import auth from './authReducer'
import translatedUrls from "./translationUrl"
import isSignInModalOpen from './signInModalOpen'
import currentMedia from "./mp_currentMedia"
import userLibrary from './userLibrary'

import mpPlayPause from "./mp_playpause"

export default combineReducers({
    breadcrumb,
    auth,
    translatedUrls,
    isSignInModalOpen,
    currentMedia,
    isAutoPlay,
    playlist,
    userLibrary,
    mpPlayPause
});