/* eslint-disable */

import React from "react"
import loadable from '@loadable/component'
import Breadcrumb from '@/layout-parts/Breadcrumb'
import shortid from 'shortid'
import TopDesktop from '@/layout-parts/Nav/TopDesktop'
import TopMobile from '@/layout-parts/Nav/TopMobile'
const SideNav = loadable(() => import('@/layout-parts/Nav/SideNav/index.tsx'))
import { menusItems } from '@/static/strings/generated/menus.json'
const Footer = loadable(() => import('@/layout-parts/Footer'))
const MediaPlayer = loadable(() => import('@/components/MediaPlayer/AudioPlayerGlobal'))
import BottomMobile from '@/layout-parts/Nav/MobileBottom'
import CookieConsent from "@/layout-parts/CookieConsent";
const SignInSignUpModal = loadable(() => import('@/layout-parts/SignInSignUp'))
import './app-wrapper.css'
const AppWrapper: React.FC = ({ children }) => {

    const [isSideNavOpen, setSideNavOpen] = React.useState(false)
    const handleSideNavOpen = (status: boolean) => {
        setSideNavOpen(status)
    }

    const NavProps = React.useMemo(() => {
        return (
            {
                isSideNavOpen,
                setSideNavOpen: handleSideNavOpen
            }
        )
    }, [
        isSideNavOpen,
        setSideNavOpen,
        handleSideNavOpen
    ])

    return (
        <>
            <TopDesktop key={shortid()} {...NavProps} explorePage={menusItems.explore} />
            <TopMobile
                {...NavProps}
                key={shortid()}
            />
            {isSideNavOpen && <SideNav {...NavProps} />}

            <CookieConsent key={shortid()} />
            <SignInSignUpModal key={shortid()} />
            <MediaPlayer key={shortid()} />

            <div className="relative layout-children">
                <Breadcrumb key={shortid()} />
                {children}

            </div>
            <BottomMobile {...NavProps} key={shortid()} />
            <Footer key={shortid()} />
        </>
    )
}

export default AppWrapper