import React from 'react'
import { useSelector } from 'react-redux'
import { IRootState } from '@/state/types'

import Dropdown from '@/components/Dropdown'
import { createSelector } from 'reselect'


const languageSelector = (state: IRootState) => ({ translatedUrls: state.translatedUrls })
const dropdownItems = createSelector(languageSelector, ({ translatedUrls }) => translatedUrls.map(i => ({ label: i.name, href: i.to })))

const Languages: React.FC<{ className?: string }> = ({ className }) => {

    const languageOptions = useSelector(dropdownItems);


    return (
        <Dropdown
            className={className}

            label={process.env.LANG ? process.env.LANG : ''}
            options={languageOptions}
            selected={undefined}
        />

    )

}

export default Languages
