import * as React from 'react';
import Link from '@/components/CustomLink'
import { HeadsetIcon, HomeIcon, ExploreIcon, DescriptionIcon, LocalOfferIcon, BookmarksIcon, PlayCircleOutlineIcon, LoginIcon } from '@/components/Icons/MUI'
import ac_strings from '@/static/strings/ac_strings'
import { INavItem } from '@/types'
import { mobile, menusItems } from '@/static/strings/generated/menus.json'
import HOCLoginCheck from '@/HOC/LogInCheck'
export interface IMenuWithIcon extends INavItem {
    icon: {
        selected: JSX.Element,
        default: JSX.Element
    }
}
interface IProps {
    isSideNavOpen: boolean
}

const iconMapNav = {
    LoginIcon,
    ExploreIcon,
    HeadsetIcon,
    DescriptionIcon,
    PlayCircleOutlineIcon,
    BookmarksIcon,
    LocalOfferIcon,
    HomeIcon
}
const BottomNavMobile: React.FC<IProps> = ({ isSideNavOpen }) => {


    let drawerClass = 'close'
    /*     if (isSideNavOpen) {
            drawerClass = 'mobile-open'
        }
     */
    let mobileMenu = mobile.default

    return (
        <div style={{ zIndex: 600 }} className={`relative w-full bottom-0  bg-white drawer-main drawer-main-${drawerClass}`}>
            {mobileMenu.map((item, i) => (
                <MenuIcon {...item} key={i} />
            ))}
            <HOCLoginCheck
                render={({ loginStatus }) => {
                    return loginStatus === "success" ? (
                        <MenuIcon
                            {...menusItems['my-content']}
                            iconName="BookmarksIcon"
                        />
                    ) : (
                            <button
                                className="flex flex-col items-center justify-between text-gray-600 flex-1 py-2 bg-gray-300"
                            >
                                <span className="flex-1 flex items-center pb-3">
                                    <LoginIcon className="fill-slate-light" />
                                </span>
                                <span className="block font-semibold" style={{ "fontSize": "10px" }}>{ac_strings.login}</span>
                            </button>
                        )
                }}
            />
        </div>


    )
}

export default BottomNavMobile

const MenuIcon: React.FC<{ to: string, name: string, iconName: string }> = ({ to, name, iconName }) => {
    const Icon = iconMapNav[iconName]
    return (
        <Link
            to={to}
            className="flex flex-col items-center justify-between text-gray-600 flex-1 py-2"
            activeClassName="bg-gray-300"
        >
            <span className="flex-1 flex items-center pb-3"><Icon className="fill-slate-light" /></span>
            <span className="block font-semibold" style={{ "fontSize": "10px" }}>{name}</span>
        </Link>
    )
}