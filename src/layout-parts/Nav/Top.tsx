import React from 'react'
import Link from '@/components/CustomLink'
import LogoFull from '@/static/images/ACLogo'
import { desktop } from '@/static/strings/generated/menus.json'
import { SearchIcon, MenuIcon } from '@/components/Icons/MUI'
import { LaunchIcon } from '@/components/Icons/MUI'
import { INavItem } from '@/types'
import ac_strings from '@/static/strings/ac_strings'
import UserNav from './User'
import LanguageDropdown from './Languages'
interface IProps {
    explorePage?: INavItem
    isSideNavOpen: boolean
    setSideNavOpen: (status: boolean) => void
}


const TopDesktop: React.FC<IProps> = ({ explorePage, isSideNavOpen, setSideNavOpen }) => {
    const tagline = {
        path: "",
        text: "bcc"
    }
    return (
        <div style={{ zIndex: 100 }} className={`fixed top-0 bg-white hidden sm:block w-full py-2 sm:py-1 border-b border-gray-200 z-50 drawer-main drawer-main-${isSideNavOpen ? 'open' : 'close'}`} >
            <div className="flex justify-between items-center border-gray-200 border-b py-2 sm:py-0 px-2 text-gray-500 text-sm ">
                <a href={tagline.path} target="_window" className="flex items-center text-sm -mt-1 pl-1">
                    <span>{tagline.text}</span>
                    <span className="px-2">
                        <LaunchIcon customSize="4" className="fill-slate-dark" />
                    </span>
                </a>
                <div className="hidden sm:flex items-center text-sm">
                    <UserNav className="pr-4" />

                    <Link className="py-2 pr-4 hover:text-ac-slate" to={ac_strings.slug_contact}>{ac_strings.contact}</Link>
                    <LanguageDropdown className="border-l pl-2 p-0" />
                </div>
                <UserNav />

            </div>
            <div className="flex py-2 standard-max-w items-center">
                <Link className='flex flex-1 justify-start items-center px-4 mt-1' to="/">
                    <LogoFull />
                </Link>
                <div className="flex">
                    {desktop.map((item, i) => (
                        <Link className="block p-2 hover:text-ac-slate-light" key={i} to={`${item.to}`}>
                            {item.name}
                        </Link>
                    ))}

                    {explorePage && (
                        <Link to={explorePage.to} className="px-2">
                            <SearchIcon customSize="6" />
                        </Link>
                    )}
                    <button className="pl-2 pr-4" onClick={() => { setSideNavOpen(!isSideNavOpen) }}>
                        <MenuIcon customSize="6" />
                    </button>
                </div>
            </div>
        </div>

    )

}

export default TopDesktop


