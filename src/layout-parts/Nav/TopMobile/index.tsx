import * as React from 'react'
import Link from '@/components/CustomLink'
import LogoFull from '@/static/images/ACLogo'
import { SearchIcon, MenuIcon } from '@/components/Icons/MUI'
import ac_strings from '@/static/strings/ac_strings.js'
import { IDrawerNav } from '@/layout/App'

import './topmobile.css'


const TopNavMobile: React.FC<IDrawerNav> = ({ isSideNavOpen, setSideNavOpen }) => {
    return (
        <div style={{ zIndex: 100 }} className={`fixed py-2 w-full top-0 z-50 sm:hidden bg-white shadow-md drawer-main drawer-main-${isSideNavOpen ? 'mobile-open' : 'close'}`}>
            <div className="flex justify-between items-center w-full">
                <Link className='px-4' to="/">
                    <LogoFull />
                </Link>
                <div className="flex items-center">
                    <Link to={ac_strings.slug_explore}>
                        <SearchIcon customSize="6" />
                    </Link>
                    <button
                        className="p-2 px-4 uppdercase text-small"
                        onClick={() => { setSideNavOpen(true) }}
                    >
                        <MenuIcon customSize="6" />
                    </button>
                </div>
            </div>


        </div>
    )
}

export default React.memo(TopNavMobile)