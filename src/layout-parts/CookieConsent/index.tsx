import * as React from 'react';
import ac_strings from '@/static/strings/ac_strings.js'
import { Button } from '@/components/Button'
import { useDispatch } from "react-redux"
import loadable from '@loadable/component'

export default () => {
    const localStorageKeyConsent = 'ac.giveConsent'

    const [showConsent, setShowConsent] = React.useState(true)

    const dispatch = useDispatch();

    const localStorageKeyLogin = 'ac.loggedIn'
    React.useEffect(() => {
        const gaveConsent = localStorage.getItem(localStorageKeyConsent)
        if (gaveConsent === "true") {
            setShowConsent(false)
        }

        const loggedIn = localStorage.getItem(localStorageKeyLogin)
        const redirectedFromFb = window.location.href && window.location.href.indexOf('#_=_') > -1

        const loadData = async () => {
            const api = await import(`@/state/apiActions`)
            api.checkUser(dispatch)
        }

        if (loggedIn === "true" || redirectedFromFb) {
            loadData();
        }




    }, [])
    const setNotIsInfoBarOpen = () => {
        localStorage.setItem(localStorageKeyConsent, "true")
        setShowConsent(false)
    }



    return showConsent ? (
        <div className="fixed bottom-0 w-full bg-ac-slate-dark text-ac-slate-lighter p-4" style={{ zIndex: 650 }}>
            <div className="standard-max-w">
                <div className="text-xs">
                    {ac_strings.consent_general_main}
                    <Button
                        className="underline pt-2"
                        to={ac_strings.slug_cookie_policy}
                    >
                        {ac_strings.consent_general_link}
                    </Button>
                </div>
                <Button
                    className="text-sm bg-ac-primary px-4 inline-block rounded-full py-1 mt-2"
                    onClick={setNotIsInfoBarOpen}
                >
                    {ac_strings.consent_general_accept}
                </Button>
            </div>

        </div>
    ) : null
}
