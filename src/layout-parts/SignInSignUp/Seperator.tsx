import React from 'react'
import ac_strings from '@/static/strings/ac_strings.js'
import "./SigninSignUp.css"
export const Seperator = () => (
    <span className="uppercase text-gray-400 text-xs separator py-2">{ac_strings.or}</span>
)