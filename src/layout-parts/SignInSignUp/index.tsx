import * as React from 'react'
import Modal from './Modal'
import { useSelector } from "react-redux";
import { IRootState } from '@/state/types'

import { createSelector } from 'reselect'
const isSignInModalOpen = (state: IRootState) => ({ status: state.isSignInModalOpen })
const signInModalSelector = createSelector(isSignInModalOpen, ({ status }) => status)


const SigninSignUpModal: React.FC = () => {
    const isSignInModalOpen = useSelector(signInModalSelector);
    return isSignInModalOpen !== null ? <Modal option={isSignInModalOpen} /> : null
}

export default SigninSignUpModal
