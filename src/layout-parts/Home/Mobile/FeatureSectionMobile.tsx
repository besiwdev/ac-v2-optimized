import React from 'react'
import { FetchPostsFromSlugs } from '@/HOC/FetchPosts'
import shortid from 'shortid'
import loadable from '@loadable/component'
const RightImgWDes = loadable(() => import('@/components/PostItemCards/RightImg'))
import { getRandomArray } from '@/helpers/normalizers'
import { ITopicPostItems, IPostItem } from '@/types'

const FeatureSection: React.FC<{ topicPosts: ITopicPostItems[] }> = ({ topicPosts }) => {

    let postSlugs: IPostItem[] = []
    topicPosts.map(t => {
        postSlugs.push(...t.posts)
    })
    const posts = getRandomArray(postSlugs, 3)
    React.useEffect(() => {

    }, [])
    return (

        <div className="px-4">
            {posts.map(item => {
                return (
                    <RightImg {...item} key={shortid()} />
                )
            })}
        </div>


    )
}

export default FeatureSection