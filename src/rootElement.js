/* eslint-disable */

import React from "react"
import { Provider } from "react-redux"
import { createStore, applyMiddleware } from "redux"
import reducers from "./state/reducer";
import {homeUrls} from "./state/reducer/translationUrl"
import loadable from '@loadable/component'
const Helmet = loadable(() => import('react-helmet'))
const AppWrapper = loadable(() => import('@/layout/AppWrapper'))
export const preloadedState = {
    auth: {
        loggedIn: 'notLoggedIn'
    },
    translatedUrls:homeUrls,
    playlist:[],
    isAutoPlay:false,
    currentMedia:{},
    mpPlayPause:false,
    isPlaying:false,
    isSignInModalOpen:null,
    userLibrary:{
        bookmarkedPosts: [],
        unfinishedPosts: [],
        followedTopics: [],
        followedPlaylists: [],
        followedAuthors: [],
        historyPosts: []
    },
    breadcrumb:{
        items:[],
        title:''
    }
}

export default ({ element }) => {
    const store = createStore(reducers, preloadedState)


    return (
        <Provider store={store}>
                <Helmet>
                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                </Helmet>
                <AppWrapper>
                {element}
                </AppWrapper>
        </Provider>
    )
}