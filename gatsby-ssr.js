/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */


import "./src/static/styles/tailwind-output.css"
import "react-placeholder/lib/reactPlaceholder.css"
import wrapWithProvider from "./src/rootElement"

export const wrapRootElement = wrapWithProvider
